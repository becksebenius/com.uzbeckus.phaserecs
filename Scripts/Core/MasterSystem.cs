﻿using System.Collections.Generic;
using UnityEngine.Profiling;

namespace Phaser
{
    public interface IMasterSystem
    {
        IReadOnlyList<IEcsSystem> ChildSystems { get; }
        TSystem GetSystem<TSystem>() where TSystem : class, IEcsSystem;
    }
    
    public class MasterSystem : IEcsSystem, IMasterSystem
    {
        public string Name { get; }
        public IReadOnlyList<IEcsSystem> ChildSystems => systems;
        
        readonly IEcsSystem[] systems;
        
        public MasterSystem(IEcsSystem[] systems)
        {
            this.systems = systems;
            Name = GetType().Name;
        }
        
        public void HandleMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            foreach (var system in systems)
            {
                Profiler.BeginSample(system.Name);
                system.HandleMessage(in message);
                Profiler.EndSample();
            }
        }

        public void Initialize(EcsInstance instance)
        {
            foreach (var system in systems)
            {
                system.Initialize(instance);
            }
        }

        public void QueuePriorityMessage<TMessage>(in TMessage message, bool unique) where TMessage : struct, IMessage
        {
            foreach (var system in systems)
            {
                system.QueuePriorityMessage(in message, unique);
            }
        }

        public void QueueMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            foreach (var system in systems)
            {
                system.QueueMessage(in message);
            }
        }

        public bool HandleQueuedMessages()
        {
            bool anyResult = false;
            
            foreach (var system in systems)
            {
                Profiler.BeginSample(system.Name);
                anyResult |= system.HandleQueuedMessages();
                Profiler.EndSample();
            }

            return anyResult;
        }
        
        public TSystem GetSystem<TSystem>() where TSystem : class, IEcsSystem
        {
            for (int i = 0; i < systems.Length; ++i)
            {
                if(systems[i] is TSystem system)
                {
                    return system;
                }
            }
            return null;
        }
    }
}
﻿namespace Phaser
{
    public abstract class SystemBase : IEcsSystem
    {
        public string Name { get; }
        protected EcsInstance EcsInstance { get; private set; }

        readonly MessageListenerCollection messageListeners = MessageListenerCollection.New();
        MessageBuffer? queuedPriorityMessageBuffer;
        MessageBuffer? queuedMessageBuffer;
        IGenericMessageListener thisAsGenericMessageListener;

        protected SystemBase()
        {
            Name = GetType().Name;
            thisAsGenericMessageListener = this as IGenericMessageListener;
        }

        protected void AddMessageListener<TMessage>(MessageListenerDelegate<TMessage> listener) where TMessage : struct, IMessage
        {
            messageListeners.AddMessageListener(listener);
        }

        void IMessageHandler.HandleMessage<TMessage>(in TMessage message) => HandleMessage(in message);
        void HandleMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            messageListeners.Invoke(message);
            thisAsGenericMessageListener?.OnMessage(in message);
        }

        void IEcsSystem.QueuePriorityMessage<TMessage>(in TMessage message, bool unique)
        {
            if (thisAsGenericMessageListener == null && !messageListeners.HasListenerForType<TMessage>())
            {
                return;
            }

            if (!queuedPriorityMessageBuffer.HasValue)
            {
                queuedPriorityMessageBuffer = MessageBuffer.New();
            }
            queuedPriorityMessageBuffer.Value.QueueMessage(in message, unique);
        }

        void IEcsSystem.QueueMessage<TMessage>(in TMessage message)
        {
            if(thisAsGenericMessageListener == null && !messageListeners.HasListenerForType<TMessage>())
            {
                return;
            }
            if(!queuedMessageBuffer.HasValue)
            {
                queuedMessageBuffer = MessageBuffer.New();
            }
            queuedMessageBuffer.Value.QueueMessage(in message, false);
        }

        bool IEcsSystem.HandleQueuedMessages()
        {
            bool anyProcessed = false;

            while (HandleQueuedPriorityMessages())
            {
                anyProcessed = true;
            }
            
            if (queuedMessageBuffer.HasValue)
            {
                while (queuedMessageBuffer.Value.HandleNext(this))
                {
                    // Handle any new priority messages that arrived during message processing
                    while (HandleQueuedPriorityMessages()){}
                    anyProcessed = true;
                }
            }

            return anyProcessed;
        }

        bool HandleQueuedPriorityMessages()
        {
            bool anyProcessed = false;

            if (queuedPriorityMessageBuffer.HasValue)
            {
                while (queuedPriorityMessageBuffer.Value.HandleNext(this))
                {
                    anyProcessed = true;
                }
            }

            return anyProcessed;
        }

        void IEcsSystem.Initialize(EcsInstance instance)
        {
            EcsInstance = instance;
            OnEcsInstanceInitialized();
        }

        protected virtual void OnEcsInstanceInitialized() {}
    }
}
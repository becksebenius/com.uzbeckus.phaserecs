﻿using System.Collections.Generic;

namespace Phaser
{
    public class MessageQueue<TMessage> : IMessageQueue
        where TMessage : struct, IMessage
    {
        readonly List<TMessage> messages = new ();

        public bool IsEmpty => messages.Count == 0;

        public void Enqueue(in TMessage message)
        {
            messages.Add(message);
        }

        public void HandleAndRemoveNext(IMessageHandler handler)
        {
            var message = messages[0];
            messages.RemoveAt(0);
            handler.HandleMessage(in message);
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace Phaser
{
    public readonly struct MessageBuffer
    {
        public static MessageBuffer New() => new (new (), new ());

        readonly Dictionary<Type, IMessageQueue> messageQueuesByType;
        readonly List<IMessageQueue> upcomingMessages;

        MessageBuffer(
            Dictionary<Type, IMessageQueue> messageQueuesByType,
            List<IMessageQueue> upcomingMessages)
        {
            this.messageQueuesByType = messageQueuesByType;
            this.upcomingMessages = upcomingMessages;
        }

        public void QueueMessage<TMessage>(in TMessage message, bool unique) where TMessage : struct, IMessage
        {
            MessageQueue<TMessage> messageQueue;
            if (messageQueuesByType.TryGetValue(typeof(TMessage), out IMessageQueue messageQueueUntyped))
            {
                messageQueue = (MessageQueue<TMessage>)messageQueueUntyped;
            }
            else
            {
                messageQueue = new MessageQueue<TMessage>();
                messageQueuesByType.Add(typeof(TMessage), messageQueue);
            }

            if (unique && !messageQueue.IsEmpty)
            {
                return;
            }

            messageQueue.Enqueue(in message);
            upcomingMessages.Add(messageQueue);
        }

        public bool HandleNext(IMessageHandler messageHandler)
        {
            if (upcomingMessages.Count == 0)
            {
                return false;
            }

            var next = upcomingMessages[0];
            upcomingMessages.RemoveAt(0);
            next.HandleAndRemoveNext(messageHandler);
            return true;
        }
    }
}
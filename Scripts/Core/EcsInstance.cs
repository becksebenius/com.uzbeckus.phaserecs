﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.Profiling;

namespace Phaser
{
    public class EcsInstance : IMessageHandler
    {
        public event Action<string> OnError;
        public IReadOnlyList<IEcsSystem> Systems => systems;

        readonly IEcsSystem[] systems;
        readonly MessageBuffer deferredMessageBuffer = MessageBuffer.New();

        public EcsInstance(IReadOnlyList<IEcsSystem> systems)
        {
            this.systems = systems.ToArray();
            for(int i = 0; i < this.systems.Length; ++i)
            {
                var system = this.systems[i];
                system.Initialize(this);
            }
        }

        /// <summary>
        /// Sends a message to be handled just-in-time when processing systems, prioritized
        /// ahead of other messages. Prioritized messages are processed in the order they're received
        /// </summary>
        public void SendPriorityMessage<TMessage>(in TMessage message, bool unique = false) where TMessage : struct, IMessage
        {
            for (int i = 0; i < systems.Length; ++i)
            {
                systems[i].QueuePriorityMessage(in message, unique);
            }
        }

        /// <summary>
        /// Sends a message to be handled just-in-time when processing systems
        /// </summary>
        public void SendMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            for (int i = 0; i < systems.Length; ++i)
            {
                systems[i].QueueMessage(in message);
            }
        }

        /// <summary>
        /// Sends a message to be handled after the current message pass is completed
        /// </summary>
        public void SendMessageDeferred<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            deferredMessageBuffer.QueueMessage(in message, false);
        }

        /// <summary>
        /// Runs an update pass on the ecs instance, which includes queuing the input message
        ///     as a deferred message and then processing all deferred messages
        /// Any pending deferred messages will be processed before the update,
        ///     and any deferred messages queued during the update pass will be run
        ///     until no more messages have been added
        /// </summary>
        public void Update<TMessage>(in TMessage message) where TMessage : struct, IMessage
        {
            SendMessageDeferred(message);

            int recursionGuard = 100;
            while (deferredMessageBuffer.HandleNext(this))
            {
                if(recursionGuard-- <= 0)
                {
                    OnError?.Invoke("100+ deferred messages processed. Probably infinite recursion!");
                    break;
                }
            }

            recursionGuard = 100;
            while (ProcessOutstandingQueuedMessages())
            {
                if(recursionGuard-- <= 0)
                {
                    OnError?.Invoke("100+ queued messages processed. Probably infinite recursion!");
                    break;
                }
            }
        }

        /// <summary>
        /// Returns the system instance in this EcsInstance
        /// </summary>
        public TSystem GetSystem<TSystem>() where TSystem : class, IEcsSystem
        {
            foreach (var system in systems)
            {
                if(system is TSystem typedSystem)
                {
                    return typedSystem;
                }
                
                if (system is IMasterSystem masterSystem)
                {
                    typedSystem = masterSystem.GetSystem<TSystem>();
                    if (typedSystem != null)
                    {
                        return typedSystem;
                    }
                }
            }

            return null;
        }

        bool ProcessOutstandingQueuedMessages()
        {
            bool anyProcessed = false;
            for(int i = 0; i < systems.Length; ++i)
            {
                var system = systems[i];
                Profiler.BeginSample(system.Name);
                anyProcessed |= system.HandleQueuedMessages();
                Profiler.EndSample();
            }
            return anyProcessed;
        }

        /// <summary>
        /// Synchronously handles the given message on all systems in the Ecs instance
        /// </summary>
        void IMessageHandler.HandleMessage<TMessage>(in TMessage message)
        {
            for (int i = 0; i < systems.Length; ++i)
            {
                var system = systems[i];
                Profiler.BeginSample(system.Name);
                system.HandleQueuedMessages();
                system.HandleMessage(in message);
                Profiler.EndSample();
            }
        }
    }
}
﻿namespace Phaser
{
    public interface IEcsSystem : IMessageHandler
    {
        string Name { get; }
        void Initialize(EcsInstance instance);
        void QueuePriorityMessage<TMessage>(in TMessage message, bool unique) where TMessage : struct, IMessage;
        void QueueMessage<TMessage>(in TMessage message) where TMessage : struct, IMessage;
        bool HandleQueuedMessages();
    }
}